﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerNro3.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerNro3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListarPersonas : ContentPage
	{
		public ListarPersonas ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {

                List<Persona> listaPersonas;
                listaPersonas = connection.Table<Persona>().ToList();

                listViewTareas.ItemsSource = listaPersonas;

            }
        }
    }
}
