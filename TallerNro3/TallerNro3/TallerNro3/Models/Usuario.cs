﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerNro3.Models
{
    class Usuario
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string NameUser { get; set; }

        [MaxLength(150)]
        public string Password { get; set; }

        [MaxLength(150)]
        public string Avatar { get; set; }

        public Boolean State { get; set; }


    }
}
