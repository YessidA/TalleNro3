﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerNro3.Models
{
    class Persona
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Phone { get; set; }

        [MaxLength(150)]
        public string Email { get; set; }

        [MaxLength(1)]
        public string Sex { get; set; }


    }
}
