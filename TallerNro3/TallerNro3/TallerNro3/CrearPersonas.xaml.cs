﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerNro3.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerNro3
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearPersonas : ContentPage
	{
		public CrearPersonas ()
		{
			InitializeComponent ();
		}

        public void ButtonClick(object sender, EventArgs e)
        {

            // crear objeto del modelo tarea
            Persona persona = new Persona()
            {
                Name = name.Text,
                Phone = phone.Text,
                Email = email.Text,
                Sex = sex.Text
            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Persona>();

                // crear registro en la tabla
                var result = connection.Insert(persona);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La persona se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La persona no fue creada", "OK");
                }
            }
        }


        async public void ListPersonas(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarPersonas());
        }


    }
}